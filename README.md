# WebSockets-based chat with Actix and Rust

This is a simple demo of a command line-based chat application that facilitates
both the client and server components, communicating over WebSockets. This is
built in Rust using the [Actix framework](https://actix.rs).

## Requirements

* Rust 1.37+

## Building
To build the application:

```bash
> cargo build
```

## Running
To run the chat server:

```bash
# Set RUST_LOG=info for less verbose output
# Bind to 127.0.0.1:8080
> RUST_LOG=debug cargo run server -h 127.0.0.1 -p 8080
```

To run a chat client:

```bash
# Set RUST_LOG=info for less verbose output
> RUST_LOG=debug cargo run client \
    --username yourusernamehere \
    --url ws://127.0.0.1:8080
```
