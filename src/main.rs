//! A simple WebSockets-based CLI chat application.

use env_logger;
use log::error;
use structopt::StructOpt;

mod chat_client;
mod chat_server;
mod messaging;
mod websocket_chat_client;
mod websocket_server;

#[derive(Debug, StructOpt)]
enum Command {
    /// Run a chat server, waiting for chat clients to connect
    Server {
        /// The chat server host to which to listen
        #[structopt(short, long, default_value = "127.0.0.1")]
        host: String,

        /// The port on which to listen
        #[structopt(short, long, default_value = "8080")]
        port: u16,
    },
    /// Run a chat client, connecting to a chat server
    Client {
        /// The WebSockets endpoint URL on which the chat server will be listening
        #[structopt(long, default_value = "ws://127.0.0.1:8080")]
        url: String,

        /// The username to use when joining the chat server.
        #[structopt(long, default_value = "chatty")]
        username: String,
    },
}

#[derive(Debug, StructOpt)]
#[structopt(name = "wschat", about = "WebSockets-based chat client/server")]
struct Opt {
    #[structopt(subcommand)]
    cmd: Command,
}

fn main() {
    env_logger::init();
    let opt = Opt::from_args();
    match opt.cmd {
        Command::Server { host, port } => {
            if let Err(e) = websocket_server::run(host.as_ref(), port) {
                error!("Failed to run WebSockets server: {}", e);
                std::process::exit(-1);
            }
        }
        Command::Client { url, username } => {
            if let Err(e) = websocket_chat_client::run(url, username) {
                error!("Failed to run chat client: {}", e);
                std::process::exit(-1);
            }
        }
    }
}
