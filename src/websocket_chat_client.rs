
use actix::*;
use actix::io::SinkWrite;
use futures::{
    lazy,
    Future,
    stream::Stream,
};
use actix_codec::{AsyncRead, AsyncWrite};
use awc::Client;
use log::{debug, info, error};
use crate::messaging::WebSocketChatMessage;
use crate::chat_client::ChatClient;
use std::{io, thread};

fn run_input_loop<T: 'static>(client: Addr<ChatClient<T>>)
where
    T: AsyncRead + AsyncWrite
{
    debug!("Spawning stdin read loop for chat");
    info!("Say \"quit\" to exit the chat at any time");
    thread::spawn(move || loop {
        let mut msg = String::new();
        if let Err(e) = io::stdin().read_line(&mut msg) {
            error!("Failed to read line from stdin: {}", e);
            return;
        }
        msg = msg.trim().to_owned();
        if msg.eq_ignore_ascii_case("quit") {
            client.do_send(WebSocketChatMessage::PoisonPill);
            return;
        }
        client.do_send(WebSocketChatMessage::BroadcastRequest{
            from_username: "".to_owned(),
            body: msg,
        });
    });
}

pub fn run(url: String, username: String) -> std::io::Result<()> {
    let sys = System::new("wschat-client");
    let _url = format!("{}/?{}", url, username);
    let _username = username.clone();

    debug!("Attempting to connect to WebSockets server: {}", _url);

    Arbiter::spawn(lazy(|| {
        Client::new()
            .ws(_url)
            .connect()
            .map_err(|e| {
                error!("Error while trying to connect to WebSockets server: {}", e);
            })
            .map(|(_response, framed)| {
                let (sink, stream) = framed.split();
                let addr = ChatClient::create(|ctx| {
                    ChatClient::add_stream(stream, ctx);
                    ChatClient::new(
                        _username,
                        SinkWrite::new(sink, ctx),
                    )
                });

                run_input_loop(addr);
            })
    }));

    sys.run()
}
