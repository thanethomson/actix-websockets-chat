//! This module contains a chat client that, right now, conflates the chat
//! dynamics with the WebSockets-based client mechanics. In a better 
//! architected version of this code, one would clearly separate the 
//! WebSockets-based interaction as a single actor, and the chat dynamics as
//! a separate actor.
//! 
//! The WebSockets client would then handle the translation of incoming 
//! WebSockets text frames into WebSocketChatMessage instances, which it would
//! then translate into ChatMessage objects to be passed to the chat client 
//! actor. That actor would, in turn, handle those messages and send outgoing
//! ChatMessage objects to the WebSockets actor, which would then convert those
//! into WebSocketsChatMessage objects and send those out as WebSockets text
//! frames.

use crate::messaging::{ChatErrorKind, WebSocketChatMessage};
use actix::{
    io::SinkWrite,
    prelude::{Actor, Context, Handler, ActorContext, AsyncContext},
    StreamHandler,
    System,
};
use actix_codec::{AsyncRead, AsyncWrite, Framed};
use awc::{
    error::WsProtocolError,
    ws::{Codec, Frame, Message},
};
use log::{debug, error, info};
use futures::stream::SplitSink;
use serde_json;
use std::str;

pub struct ChatClient<T>
where
    T: AsyncRead + AsyncWrite
{
    username: String,
    ready: bool,
    sink_write: SinkWrite<SplitSink<Framed<T, Codec>>>,
}

impl<T: 'static> ChatClient<T>
where
    T: AsyncRead + AsyncWrite
{
    pub fn new(username: String, sink_write: SinkWrite<SplitSink<Framed<T, Codec>>>) -> Self {
        ChatClient{
            username,
            ready: false,
            sink_write,
        }
    }

    fn broadcast(&mut self, body: String) {
        let msg = WebSocketChatMessage::BroadcastRequest{
            from_username: self.username.clone(),
            body: body,
        };
        match serde_json::to_string(&msg) {
            Ok(m) => if let Err(e) = self.sink_write.write(Message::Text(m)) {
                error!("Failed to write outgoing message: {}", e);
            },
            Err(e) => error!("Failed to serialize outgoing message: {}", e),
        }
    }

    fn send_pong(&mut self) {
        debug!("Sending PONG to WebSockets server");
        if let Err(e) = self.sink_write.write(Message::Pong("PONG".to_owned())) {
            error!("Failed sending PONG message: {}", e);
        }
    }

    fn handle_text_message(&mut self, txt: String, ctx: &mut Context<Self>) {
        debug!("Got incoming message from WebSockets server: {}", txt);
        match serde_json::from_str(txt.as_ref()) {
            Ok(m) => ctx.address().do_send(m),
            Err(e) => error!("Failed to decode incoming message from WebSockets server: {}", e),
        }
    }

    fn handle_welcome(&mut self) {
        info!("Received welcome message from server");
        self.ready = true;
    }

    fn handle_incoming_broadcast(&self, from_username: String, body: String) {
        if !self.ready {
            error!("Cannot handle incoming messages since we haven't been welcomed by the server yet");
            return;
        }

        // for now we just log this out
        info!("{}: {}", from_username, body);
    }

    fn handle_error(&self, kind: ChatErrorKind, message: String) {
        error!("{}: {}", kind, message);
    }

    fn handle_poison_pill(&mut self) {
        debug!("Got poison pill");
        info!("Shutting down");
        System::current().stop();
    }

}

impl<T: 'static> Actor for ChatClient<T>
where
    T: AsyncRead + AsyncWrite
{
    type Context = Context<Self>;
}

impl<T: 'static> Handler<WebSocketChatMessage> for ChatClient<T>
where
    T: AsyncRead + AsyncWrite
{
    type Result = ();

    fn handle(&mut self, msg: WebSocketChatMessage, _: &mut Context<Self>) {
        debug!("Got message: {:?}", msg.clone());
        match msg {
            WebSocketChatMessage::Welcome => self.handle_welcome(),

            WebSocketChatMessage::BroadcastRequest {
                from_username: _,
                body,
            } => self.broadcast(body),

            WebSocketChatMessage::Broadcast {
                from_username,
                body,
            } => self.handle_incoming_broadcast(from_username, body),

            WebSocketChatMessage::Error { 
                kind, 
                message,
            } => self.handle_error(kind, message),

            WebSocketChatMessage::PoisonPill => self.handle_poison_pill(),
        }
    }
}

impl<T: 'static> StreamHandler<Frame, WsProtocolError> for ChatClient<T>
where
    T: AsyncRead + AsyncWrite
{
    fn handle(&mut self, msg: Frame, ctx: &mut Context<Self>) {
        match msg {
            Frame::Text(bytes_opt) => if let Some(bytes) = bytes_opt {
                match str::from_utf8(bytes.as_ref()) {
                    Ok(s) => self.handle_text_message(s.to_owned(), ctx),
                    Err(e) => error!("Failed to interpret incoming WebSockets text frame: {}", e),
                }
            },
            Frame::Ping(_) => self.send_pong(),
            _ => (),
        }
    }

    fn started(&mut self, _: &mut Context<Self>) {
        info!("Connected to server");
    }

    fn finished(&mut self, ctx: &mut Context<Self>) {
        info!("Server disconnected");
        ctx.stop()
    }
}

impl<T: 'static> actix::io::WriteHandler<WsProtocolError> for ChatClient<T> 
where
    T: AsyncRead + AsyncWrite
{   
}
