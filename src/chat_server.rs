use crate::messaging::{ChatErrorKind, ChatMessage};
use actix::prelude::{Actor, Context, Handler, Recipient};
use log::{debug, error};
use std::collections::HashMap;

/// `ChatServer` is an actor that does not know anything about the underlying transport mechanism
/// used to communicate with participants. Each participant is also modelled as an actor.
pub struct ChatServer {
    participants: HashMap<String, Recipient<ChatMessage>>,
}

impl ChatServer {
    /// Create a new chat server with an empty set of participants.
    pub fn new() -> Self {
        ChatServer {
            participants: HashMap::new(),
        }
    }

    /// Broadcast the given message to all participants, optionally providing a sender's username.
    /// The only sender that could opt out of providing a username would be the system.
    fn broadcast(&self, from_username: Option<String>, body: String) {
        let msg = ChatMessage::Broadcast {
            from_username,
            body,
        };
        for (_, participant) in self.participants.iter() {
            let _ = participant.do_send(msg.clone());
        }
    }

    fn add_participant(&mut self, username: String, addr: Recipient<ChatMessage>) {
        // if the username's already been taken
        if let Some(_) = self.participants.get(username.as_str()) {
            let _ = addr.do_send(ChatMessage::Error {
                kind: ChatErrorKind::UsernameTaken,
                message: format!("username {} has already been taken", username.as_str()),
            });
            return;
        }
        let _ = self.participants.insert(username, addr.clone());
        let _ = addr.do_send(ChatMessage::Welcome);
    }

    fn remove_participant(&mut self, username: String) {
        let _ = self.participants.remove(username.as_str());
    }
}

impl Actor for ChatServer {
    type Context = Context<Self>;
}

impl Handler<ChatMessage> for ChatServer {
    type Result = ();

    fn handle(&mut self, msg: ChatMessage, _: &mut Self::Context) {
        debug!("Got message: {:?}", msg.clone());
        match msg {
            ChatMessage::Join { username, addr } => self.add_participant(username, addr),
            ChatMessage::Leave { username } => self.remove_participant(username),
            ChatMessage::BroadcastRequest {
                from_username,
                body,
            } => self.broadcast(from_username, body),
            _ => error!("Unexpected message type!"),
        }
    }
}
