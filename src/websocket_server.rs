use crate::chat_server::ChatServer;
use crate::messaging::{ChatMessage, WebSocketChatMessage};
use actix::prelude::Future;
use actix::*;
use actix_web::error::ErrorBadRequest;
use actix_web::{web, App, Error, HttpRequest, HttpResponse, HttpServer};
use actix_web_actors::ws;
use log::{debug, error, info};
use std::time::{Duration, Instant};

const HEARTBEAT_INTERVAL: Duration = Duration::from_secs(5);
const CLIENT_TIMEOUT: Duration = Duration::from_secs(15);

/// `WebSocketChatSession` is an actor that manages communication with an external client via
/// WebSockets.
pub struct WebSocketChatSession {
    /// When last did we hear from the client?
    pub last_seen: Instant,

    /// The client's desired username.
    pub username: String,

    /// The chat server to which to transmit incoming client messages and from which to relay
    /// responses.
    pub server: Addr<ChatServer>,
}

impl Actor for WebSocketChatSession {
    type Context = ws::WebsocketContext<Self>;

    fn started(&mut self, ctx: &mut Self::Context) {
        self.send_heartbeat(ctx);

        // try to join the chat server
        let addr = ctx.address();
        let result = self
            .server
            .send(ChatMessage::Join {
                addr: addr.recipient(),
                username: self.username.clone(),
            })
            .wait();
        if let Err(e) = result {
            println!("Got error while trying to join: {}", e);
        }
    }

    fn stopping(&mut self, _: &mut Self::Context) -> Running {
        self.server.do_send(ChatMessage::Leave {
            username: self.username.clone(),
        });
        Running::Stop
    }
}

impl WebSocketChatSession {
    fn handle_incoming(&self, msg: WebSocketChatMessage) {
        let _ = self.server.do_send(ChatMessage::from(msg));
    }

    fn send_heartbeat(&self, ctx: &mut ws::WebsocketContext<Self>) {
        ctx.run_interval(HEARTBEAT_INTERVAL, |act, ctx| {
            if Instant::now().duration_since(act.last_seen) > CLIENT_TIMEOUT {
                error!("Failed to see user recently enough: {}", &act.username);
                act.server.do_send(ChatMessage::Leave {
                    username: act.username.clone(),
                });
                ctx.stop();
                return;
            }
            debug!("Sending PING to remote: {}", &act.username);
            ctx.ping("PING");
        });
    }
}

/// Handle messages from chat server - these just get serialized and relayed to the peer's WebSocket connection.
impl Handler<ChatMessage> for WebSocketChatSession {
    type Result = ();

    fn handle(&mut self, msg: ChatMessage, ctx: &mut Self::Context) {
        let ws_msg = WebSocketChatMessage::from(msg.clone());
        match serde_json::to_string(&ws_msg) {
            Ok(m) => ctx.text(m),
            Err(e) => error!("Failed to serialize outgoing message: {}", e),
        }
    }
}

impl StreamHandler<ws::Message, ws::ProtocolError> for WebSocketChatSession {
    fn handle(&mut self, msg: ws::Message, ctx: &mut Self::Context) {
        debug!("Got incoming message: {:?}", &msg);
        self.last_seen = Instant::now();
        match msg {
            ws::Message::Ping(_) => ctx.pong("PONG"),
            ws::Message::Pong(_) => debug!("Got PONG from remote: {}", &self.username),
            ws::Message::Text(txt) => {
                debug!("Got incoming text: {}", txt.clone());
                let msg: Result<WebSocketChatMessage, serde_json::Error> =
                    serde_json::from_str(txt.as_str());
                match msg {
                    Ok(m) => self.handle_incoming(m),
                    Err(e) => error!("{}", e),
                }
            }
            ws::Message::Binary(_) => error!("Unexpected incoming binary message"),
            ws::Message::Close(_) => {
                debug!("Got close message from remote: {}", &self.username);
                ctx.stop()
            }
            ws::Message::Nop => (),
        }
    }
}

/// ws://127.0.0.1:8080/?my_username
fn chat_route(
    req: HttpRequest,
    stream: web::Payload,
    srv: web::Data<Addr<ChatServer>>,
) -> Result<HttpResponse, Error> {
    match req.uri().query() {
        Some(query) => {
            // kick off the websockets-based chat session
            ws::start(
                WebSocketChatSession {
                    last_seen: Instant::now(),
                    username: query.to_owned(),
                    server: srv.get_ref().clone(),
                },
                &req,
                stream,
            )
        }
        None => Err(ErrorBadRequest("missing query string in incoming request")),
    }
}

/// `run` will configure and run the entire actor system for the WebSockets-based chat server.
pub fn run(host: &str, port: u16) -> std::io::Result<()> {
    let sys = System::new("wschat-server");
    let server = ChatServer::new().start();

    info!(
        "Starting WebSockets-based chat server bound to {}:{}",
        host, port
    );
    HttpServer::new(move || {
        App::new()
            .data(server.clone())
            .service(web::resource("/").to(chat_route))
    })
    .bind(format!("{}:{}", host, port))?
    .start();

    sys.run()
}
