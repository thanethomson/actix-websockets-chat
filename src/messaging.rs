use actix::prelude::{Message, Recipient};
use serde::{Deserialize, Serialize};
use std::fmt;

/// `ChatErrorKind` allows us to model different kinds of errors we could send to chat room
/// participants.
#[derive(Clone, Debug, Serialize, Deserialize)]
pub enum ChatErrorKind {
    UsernameTaken,
    InternalServerError,
}

impl fmt::Display for ChatErrorKind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ChatErrorKind::UsernameTaken => write!(f, "UsernameTaken"),
            ChatErrorKind::InternalServerError => write!(f, "InternalServerError"),
        }
    }
}

/// `ChatMessage` provides a way to model a variety of different kinds of messages that could be
/// sent between the chat server and participants.
#[derive(Message, Clone)]
pub enum ChatMessage {
    Join {
        addr: Recipient<ChatMessage>,
        username: String,
    },
    Welcome,
    Leave {
        username: String,
    },
    /// `BroadcastRequest` is a request to the server to broadcast a message.
    BroadcastRequest {
        from_username: Option<String>,
        body: String,
    },
    /// `Broadcast` is a broadcast message sent by the server (a response to a successful
    /// `BroadcastRequest` message).
    Broadcast {
        from_username: Option<String>,
        body: String,
    },
    Error {
        kind: ChatErrorKind,
        message: String,
    },
}

impl fmt::Debug for ChatMessage {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        match self {
            ChatMessage::Join { addr: _, username } => {
                write!(f, "ChatMessage::Join {{ username: {} }}", username.clone())
            }
            ChatMessage::Welcome => write!(f, "ChatMessage::Welcome"),
            ChatMessage::Leave { username } => {
                write!(f, "ChatMessage::Leave {{ username: {} }}", username.clone())
            }
            ChatMessage::BroadcastRequest {
                from_username,
                body,
            } => match from_username {
                Some(u) => write!(
                    f,
                    "ChatMessage::BroadcastRequest {{ from_username: {}, body: {} }}",
                    u.clone(),
                    body.clone(),
                ),
                None => write!(f, "ChatMessage::BroadcastRequest {{ body: {} }}", body.clone()),
            },
            ChatMessage::Broadcast {
                from_username,
                body,
            } => match from_username {
                Some(u) => write!(
                    f,
                    "ChatMessage::Broadcast {{ from_username: {}, body: {} }}",
                    u.clone(),
                    body.clone(),
                ),
                None => write!(f, "ChatMessage::Broadcast {{ body: {} }}", body.clone()),
            },
            ChatMessage::Error { kind, message } => write!(
                f,
                "ChatMessage::Error {{ kind: {}, message: {} }}",
                kind.clone(),
                message.clone()
            ),
        }
    }
}

#[derive(Message, Clone, Debug, Serialize, Deserialize)]
#[serde(tag = "type")]
pub enum WebSocketChatMessage {
    Welcome,
    BroadcastRequest {
        from_username: String,
        body: String,
    },
    Broadcast {
        from_username: String,
        body: String,
    },
    Error {
        kind: ChatErrorKind,
        message: String,
    },
    PoisonPill,
}

impl From<ChatMessage> for WebSocketChatMessage {
    fn from(msg: ChatMessage) -> Self {
        match msg {
            ChatMessage::Welcome => WebSocketChatMessage::Welcome,
            ChatMessage::BroadcastRequest {
                from_username,
                body,
            } => match from_username {
                Some(u) => WebSocketChatMessage::BroadcastRequest {
                    from_username: u,
                    body,
                },
                None => WebSocketChatMessage::BroadcastRequest {
                    from_username: "system".to_owned(),
                    body,
                },
            },
            ChatMessage::Broadcast {
                from_username,
                body,
            } => match from_username {
                Some(u) => WebSocketChatMessage::Broadcast {
                    from_username: u,
                    body,
                },
                None => WebSocketChatMessage::Broadcast {
                    from_username: "system".to_owned(),
                    body,
                },
            },
            ChatMessage::Error { kind, message } => WebSocketChatMessage::Error { kind, message },
            _ => WebSocketChatMessage::Error {
                kind: ChatErrorKind::InternalServerError,
                message: format!("Cannot convert message to WebSocketChatMessage: {:?}", msg),
            },
        }
    }
}

impl From<WebSocketChatMessage> for ChatMessage {
    fn from(msg: WebSocketChatMessage) -> Self {
        match msg {
            WebSocketChatMessage::Welcome => ChatMessage::Welcome,
            WebSocketChatMessage::BroadcastRequest {
                from_username,
                body,
            } => ChatMessage::BroadcastRequest {
                from_username: Some(from_username),
                body,
            },
            WebSocketChatMessage::Broadcast {
                from_username,
                body,
            } => ChatMessage::Broadcast {
                from_username: Some(from_username),
                body,
            },
            _ => ChatMessage::Error {
                kind: ChatErrorKind::InternalServerError,
                message: "".to_owned(),
            },
        }
    }
}
